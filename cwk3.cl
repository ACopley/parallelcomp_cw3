// Kernel for matrix transposition.
__kernel
void transpose(__global float *hostMatrix, __global float *transposedMatrix, int nRows, int nCols )
{
  int gid = get_global_id(0);
  transposedMatrix[(gid % nCols) * nRows + (gid / nCols)] = hostMatrix[gid];
}
